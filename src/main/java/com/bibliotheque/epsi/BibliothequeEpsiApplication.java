package com.bibliotheque.epsi;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class BibliothequeEpsiApplication {

	public static void main(String[] args) {
		SpringApplication.run(BibliothequeEpsiApplication.class, args);
	}

}
