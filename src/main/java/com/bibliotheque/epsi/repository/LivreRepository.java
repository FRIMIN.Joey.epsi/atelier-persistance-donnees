package com.bibliotheque.epsi.repository;

import com.bibliotheque.epsi.entity.Livre;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface LivreRepository extends JpaRepository<Livre, Long> {
    @Query("SELECT l, a.id as auteurId, c.id as categorieId FROM Livre l LEFT JOIN l.auteur a LEFT JOIN l.categorie c")
    List<Object[]> findAllWithDetails();
}
