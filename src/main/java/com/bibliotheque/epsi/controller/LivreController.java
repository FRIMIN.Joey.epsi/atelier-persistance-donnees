package com.bibliotheque.epsi.controller;

import com.bibliotheque.epsi.entity.Livre;
import com.bibliotheque.epsi.repository.LivreRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.*;

@RestController
@RequestMapping("/livres")
public class LivreController {

    @Autowired
    private LivreRepository livreRepository;

    @GetMapping
    public List<Map<String, Object>> getAllLivres() {
        List<Object[]> livresWithDetails = livreRepository.findAllWithDetails();
        List<Map<String, Object>> livres = new ArrayList<>();
        for (Object[] livreWithDetails : livresWithDetails) {
            Livre livre = (Livre) livreWithDetails[0];
            Long auteurId = (Long) livreWithDetails[1];
            Long categorieId = (Long) livreWithDetails[2];
            Map<String, Object> livreMap = new HashMap<>();
            livreMap.put("id", livre.getId());
            livreMap.put("titre", livre.getTitre());
            livreMap.put("dateDeParution", livre.getDateDeParution());
            livreMap.put("nombreDePages", livre.getNombreDePages());
            livreMap.put("auteurId", auteurId);
            livreMap.put("categorieId", categorieId);
            livres.add(livreMap);
        }
        return livres;
    }


    @GetMapping("/{id}")
    public ResponseEntity<Livre> getLivreById(@PathVariable(value = "id") Long livreId) {
        Optional<Livre> livre = livreRepository.findById(livreId);
        return livre.map(ResponseEntity::ok).orElse(ResponseEntity.notFound().build());
    }

    @PostMapping
    public Livre createLivre(@RequestBody Livre livre) {
        return livreRepository.save(livre);
    }

    @PutMapping("/{id}")
    public ResponseEntity<Livre> updateLivre(@PathVariable(value = "id") Long livreId, @RequestBody Livre livreDetails) {
        Optional<Livre> livre = livreRepository.findById(livreId);
        if (livre.isEmpty()) {
            return ResponseEntity.notFound().build();
        }
        livre.get().setTitre(livreDetails.getTitre());
        livre.get().setDateDeParution(livreDetails.getDateDeParution());
        livre.get().setNombreDePages(livreDetails.getNombreDePages());
        Livre updatedLivre = livreRepository.save(livre.get());
        return ResponseEntity.ok(updatedLivre);
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<Void> deleteLivre(@PathVariable(value = "id") Long livreId) {
        Optional<Livre> livre = livreRepository.findById(livreId);
        if (livre.isEmpty()) {
            return ResponseEntity.notFound().build();
        }
        livreRepository.delete(livre.get());
        return ResponseEntity.ok().build();
    }
}
