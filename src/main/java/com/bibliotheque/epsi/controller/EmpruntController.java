package com.bibliotheque.epsi.controller;

import com.bibliotheque.epsi.entity.Emprunt;
import com.bibliotheque.epsi.repository.EmpruntRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/emprunts")
public class EmpruntController {

    @Autowired
    private EmpruntRepository empruntRepository;

    @GetMapping
    public List<Emprunt> getAllEmprunts() {
        return empruntRepository.findAll();
    }

    @GetMapping("/{id}")
    public ResponseEntity<Emprunt> getEmpruntById(@PathVariable(value = "id") Long empruntId) {
        Optional<Emprunt> emprunt = empruntRepository.findById(empruntId);
        return emprunt.map(ResponseEntity::ok).orElse(ResponseEntity.notFound().build());
    }

    @PostMapping
    public Emprunt createEmprunt(@RequestBody Emprunt emprunt) {
        if (emprunt.getDateEmprunt() == null) {
            emprunt.setDateEmprunt(LocalDate.from(LocalDateTime.now()));
        }
        return empruntRepository.save(emprunt);
    }

    @PutMapping("/{id}")
    public ResponseEntity<Emprunt> updateEmprunt(@PathVariable(value = "id") Long empruntId, @RequestBody Emprunt empruntDetails) {
        Optional<Emprunt> emprunt = empruntRepository.findById(empruntId);
        if (emprunt.isEmpty()) {
            return ResponseEntity.notFound().build();
        }
        emprunt.get().setDateEmprunt(empruntDetails.getDateEmprunt());
        emprunt.get().setDateFinPrevue(empruntDetails.getDateFinPrevue());
        emprunt.get().setDateRetour(empruntDetails.getDateRetour());
        Emprunt updatedEmprunt = empruntRepository.save(emprunt.get());
        return ResponseEntity.ok(updatedEmprunt);
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<Void> deleteEmprunt(@PathVariable(value = "id") Long empruntId) {
        Optional<Emprunt> emprunt = empruntRepository.findById(empruntId);
        if (emprunt.isEmpty()) {
            return ResponseEntity.notFound().build();
        }
        empruntRepository.delete(emprunt.get());
        return ResponseEntity.ok().build();
    }
}

