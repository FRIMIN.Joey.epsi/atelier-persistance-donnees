package com.bibliotheque.epsi.controller;

import com.bibliotheque.epsi.entity.Categorie;
import com.bibliotheque.epsi.repository.CategorieRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/categories")
public class CategorieController {

    @Autowired
    private CategorieRepository categorieRepository;

    @GetMapping
    public List<Categorie> getAllCategories() {
        return categorieRepository.findAll();
    }

    @GetMapping("/{id}")
    public ResponseEntity<Categorie> getCategorieById(@PathVariable(value = "id") Long categorieId) {
        Optional<Categorie> categorie = categorieRepository.findById(categorieId);
        return categorie.map(ResponseEntity::ok).orElse(ResponseEntity.notFound().build());
    }

    @PostMapping
    public Categorie createCategorie(@RequestBody Categorie categorie) {
        return categorieRepository.save(categorie);
    }

    @PutMapping("/{id}")
    public ResponseEntity<Categorie> updateCategorie(@PathVariable(value = "id") Long categorieId, @RequestBody Categorie categorieDetails) {
        Optional<Categorie> categorie = categorieRepository.findById(categorieId);
        if (categorie.isEmpty()) {
            return ResponseEntity.notFound().build();
        }
        categorie.get().setNom(categorieDetails.getNom());
        Categorie updatedCategorie = categorieRepository.save(categorie.get());
        return ResponseEntity.ok(updatedCategorie);
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<Void> deleteCategorie(@PathVariable(value = "id") Long categorieId) {
        Optional<Categorie> categorie = categorieRepository.findById(categorieId);
        if (categorie.isEmpty()) {
            return ResponseEntity.notFound().build();
        }
        categorieRepository.delete(categorie.get());
        return ResponseEntity.ok().build();
    }
}

