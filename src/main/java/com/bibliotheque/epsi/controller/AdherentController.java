package com.bibliotheque.epsi.controller;

import com.bibliotheque.epsi.entity.Adherent;
import com.bibliotheque.epsi.repository.AdherentRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/adherents")
public class AdherentController {

    @Autowired
    private AdherentRepository adherentRepository;

    @GetMapping
    public List<Adherent> getAllAdherents() {
        return adherentRepository.findAll();
    }

    @GetMapping("/{id}")
    public ResponseEntity<Adherent> getAdherentById(@PathVariable(value = "id") Long adherentId) {
        Optional<Adherent> adherent = adherentRepository.findById(adherentId);
        return adherent.map(ResponseEntity::ok).orElse(ResponseEntity.notFound().build());
    }

    @PostMapping
    public Adherent createAdherent(@RequestBody Adherent adherent) {
        return adherentRepository.save(adherent);
    }

    @PutMapping("/{id}")
    public ResponseEntity<Adherent> updateAdherent(@PathVariable(value = "id") Long adherentId, @RequestBody Adherent adherentDetails) {
        Optional<Adherent> adherent = adherentRepository.findById(adherentId);
        if (adherent.isEmpty()) {
            return ResponseEntity.notFound().build();
        }
        adherent.get().setNom(adherentDetails.getNom());
        adherent.get().setPrenom(adherentDetails.getPrenom());
        adherent.get().setEmail(adherentDetails.getEmail());
        adherent.get().setDateInscription(adherentDetails.getDateInscription());
        Adherent updatedAdherent = adherentRepository.save(adherent.get());
        return ResponseEntity.ok(updatedAdherent);
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<Void> deleteAdherent(@PathVariable(value = "id") Long adherentId) {
        Optional<Adherent> adherent = adherentRepository.findById(adherentId);
        if (adherent.isEmpty()) {
            return ResponseEntity.notFound().build();
        }
        adherentRepository.delete(adherent.get());
        return ResponseEntity.ok().build();
    }
}

