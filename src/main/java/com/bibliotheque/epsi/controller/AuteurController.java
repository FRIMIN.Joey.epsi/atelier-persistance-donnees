package com.bibliotheque.epsi.controller;

import com.bibliotheque.epsi.entity.Auteur;
import com.bibliotheque.epsi.repository.AuteurRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/auteurs")
public class AuteurController {

    @Autowired
    private AuteurRepository auteurRepository;

    @GetMapping
    public List<Auteur> getAllAuteurs() {
        return auteurRepository.findAll();
    }

    @GetMapping("/{id}")
    public ResponseEntity<Auteur> getAuteurById(@PathVariable(value = "id") Long auteurId) {
        Optional<Auteur> auteur = auteurRepository.findById(auteurId);
        return auteur.map(ResponseEntity::ok).orElse(ResponseEntity.notFound().build());
    }

    @PostMapping
    public Auteur createAuteur(@RequestBody Auteur auteur) {
        return auteurRepository.save(auteur);
    }

    @PutMapping("/{id}")
    public ResponseEntity<Auteur> updateAuteur(@PathVariable(value = "id") Long auteurId, @RequestBody Auteur auteurDetails) {
        Optional<Auteur> auteur = auteurRepository.findById(auteurId);
        if (auteur.isEmpty()) {
            return ResponseEntity.notFound().build();
        }
        auteur.get().setNom(auteurDetails.getNom());
        auteur.get().setPrenom(auteurDetails.getPrenom());
        Auteur updatedAuteur = auteurRepository.save(auteur.get());
        return ResponseEntity.ok(updatedAuteur);
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<Void> deleteAuteur(@PathVariable(value = "id") Long auteurId) {
        Optional<Auteur> auteur = auteurRepository.findById(auteurId);
        if (auteur.isEmpty()) {
            return ResponseEntity.notFound().build();
        }
        auteurRepository.delete(auteur.get());
        return ResponseEntity.ok().build();
    }
}

