# Spring Boot Sample

Spring Boot Web application to provide REST API in JSON

## 1. Getting started

### 1.1. Récupérer les sources

```
$ git clone https://gitlab.com/FRIMIN.Joey.epsi/atelier-persistance-donnees.git
```

### 1.2. Lancer l'application

```
$ mvn spring-boot:run
```

### 1.3. API
```
Voir collection POSTMAN -> Racine du projet Bibliothèque API.postman_collection.json
```